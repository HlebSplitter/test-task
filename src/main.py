from fastapi import FastAPI, Depends, HTTPException, APIRouter

from src.core.settings import settings
from src.handlers.user import router as user_router
from src.handlers.profile import router as profile_router

app = FastAPI()


router = APIRouter()

router.include_router(user_router, prefix="/users", tags=["users"])
router.include_router(profile_router, prefix="/profile", tags=["profile"])


app.include_router(router)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run("main:app", host=settings.host, port=settings.port, reload=settings.debug)
