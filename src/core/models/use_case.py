from sqlalchemy import Column, Integer, String
from .base import Base


class UseCase(Base):
    __tablename__ = 'use_cases'

    id = Column(Integer, primary_key=True)
    case_type = Column(String)
