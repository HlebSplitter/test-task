from sqlalchemy import Column, Integer, String, select

from src.core.models.base import Base


class UserModel(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    email = Column(String)

    @classmethod
    async def get_user_by_id(cls, user_id, conn):
        query = select(cls).where(cls.id == user_id)
        result = await conn.execute(query)
        user = result.scalars().first()
        return user

