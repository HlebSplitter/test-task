from sqlalchemy import Column, Integer, ForeignKey, select, Sequence, update, insert
from sqlalchemy.orm import relationship
from .base import Base
from ..schemas.user import UserProfileCreate
from .role import Role
from .use_case import UseCase
from .company_size import CompanySize


class UserProfileModel(Base):
    __tablename__ = 'user_profiles'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    company_size_id = Column(Integer, ForeignKey('company_sizes.id'))
    use_case_id = Column(Integer, ForeignKey('use_cases.id'))
    role_id = Column(Integer, ForeignKey('roles.id'))

    @classmethod
    async def get_user_profile(cls, user_id, conn):
        query = select(cls).where(cls.user_id == user_id)
        result = await conn.execute(query)
        user = result.scalars().first()
        return user

    @classmethod
    async def create_profile(cls, user_id: int, profile: UserProfileCreate, conn):
        conn.add(cls(**profile.model_dump(), user_id=user_id))
        await conn.commit()

    @classmethod
    async def update_profile(cls, profile: UserProfileCreate, conn):
        query = update(cls).where(cls.user_id == profile.user_id).values(
            company_size_id=profile.company_size_id,
            use_case_id=profile.use_case_id,
            role_id=profile.role_id
        )
        await conn.execute(query)
