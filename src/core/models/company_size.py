from sqlalchemy import Column, Integer, String
from .base import Base


class CompanySize(Base):
    __tablename__ = 'company_sizes'

    id = Column(Integer, primary_key=True)
    size_range = Column(String)
