from pydantic import BaseModel


class UserBase(BaseModel):
    email: str
    name: str


class UserCreate(UserBase):
    pass


class User(UserBase):
    id: int

    class Config:
        from_attributes = True


class UserProfileBase(BaseModel):
    company_size_id: int
    use_case_id: int
    role_id: int


class UserProfileCreate(UserProfileBase):
    pass


class UserProfile(UserProfileBase):
    pass

