from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine

from src.core.settings import Settings

settings = Settings()
engine = create_async_engine(settings.database_url)
async_session = async_sessionmaker(engine, expire_on_commit=False)
