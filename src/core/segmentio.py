import segment.analytics as analytics

from src.core.settings import settings

analytics.write_key = settings.segmentio_key
