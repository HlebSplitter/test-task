from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    host: str = 'localhost'
    port: int = 8000
    debug: bool = False

    database_url: str
    segmentio_key: str

    class Config:
        env_file = '.env'


settings = Settings()
