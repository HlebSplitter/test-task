from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession

from src.core.dependencies.db import get_db
from src.core.models.user_profile import UserProfileModel
from src.core.schemas.user import User, UserProfileCreate, UserProfile

from src.core.segmentio import analytics

router = APIRouter()


# instead user_id in url we should take authed user and get his id
# but as we don't have auth yet, I do it like this
# should be: async def read_user(user: User = Depends(get_user), db: AsyncSession = Depends(get_db)):
# where get_user is a dependency that returns authed user
@router.post("/{user_id}", response_model=UserProfile)
async def fill_profile(user_id: int, profile: UserProfileCreate, db: AsyncSession = Depends(get_db)):
    user_profile = await UserProfileModel.get_user_profile(user_id=user_id, conn=db)
    if user_profile:
        raise HTTPException(status_code=400, detail="User filled profile already")
    await UserProfileModel.create_profile(user_id=user_id, profile=profile, conn=db)
    analytics.identify(user_id, traits={'use_cases': profile.use_case_id, 'role': profile.role_id,
                                        'company_size': profile.company_size_id})
    return profile


@router.get("/{user_id}", response_model=UserProfile)
async def get_profile(user_id: int, db: AsyncSession = Depends(get_db)):
    user_profile = await UserProfileModel.get_user_profile(user_id=user_id, conn=db)
    if not user_profile:
        raise HTTPException(status_code=404, detail="User profile not found")
    return user_profile
