from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession

from src.core.dependencies.db import get_db
from src.core.models.user import UserModel
from src.core.schemas.user import User

router = APIRouter()


@router.get("/{user_id}", response_model=User)
async def read_user(user_id: int, db: AsyncSession = Depends(get_db)):
    db_user = await UserModel.get_user_by_id(user_id=user_id, conn=db)
    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")
    return db_user
