# Test task

## Install

### Requirements
* Python 3.11
* Poetry

### Installation

Rename `.env.example` to `.env` and fill it with your data.

```bash
cp .env.example .env
nano .env
```

Install dependencies and run the project.

```bash
poetry install
python src/main.py
```

## Usage

Documentation is available at http://localhost:8000/docs

